export interface Song {
  title: string;
  author: string;
  album: string;
  releasedate: any;
  id?: number;
  source: string;
  audioSrc: string;
}
