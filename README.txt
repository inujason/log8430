Bienvenue dans l'application MusicPlayer!

Prerequis:
    npm

Les etapes a suivre pour faire rouler notre application:

    Lancer le serveur avant l'application est preferable, mais il faut obligatoirement
    lancer les deux pour que le système marche.

    Etapes pour rouler le serveur express
        1. Ouvrir un terminal
        2. Aller dans le dossier Server
        3. Executer la commande "npm install"
        4. Executer la commande "npm run watch"

    Etapes pour rouler l'application client Angular
        1. Ouvrir un autre terminal (ou un nouvel onglet)
        2. Aller dans le dossier MusicPlayer
        3. Executer la commande "npm install"
        4. Executer la commande "ng serve —-open"
        